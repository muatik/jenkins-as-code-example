pipelineJob('my-second-app-pipeline') {
  definition {
    cps {
      sandbox()
      script("""
        node {
          stage('build') {
          	build 'my-second-app-version'
            build 'my-second-app-package'
          }
          stage('test') {
            build 'my-second-app-unittest'
            build 'my-second-app-smoke'
          }
          stage('deploy') {
            build 'my-second-app-deploy-staging'
          }
        }
      """.stripIndent())      
    }
  }
}
