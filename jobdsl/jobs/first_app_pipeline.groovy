pipelineJob('my-first-app-pipeline') {
  triggers {
        // scm('H/2 * * * *') 
        // polling is not good but okay in the purpuse of learning.
        // use scm hook instead.
  }
  definition {
    cps {
      sandbox()
      script("""
        node {
          stage('build') {
          	build 'my-first-app-version'
            build 'my-first-app-package'
          }
          stage('test') {
            build 'my-first-app-unittest'
            build 'my-first-app-smoke'
          }
          stage('deploy-staging') {
            build 'my-first-app-deploy-staging'
          }
          stage('deploy-production') {
            build 'my-first-app-deploy-production'
          }
        }
      """.stripIndent())      
    }
  }
}
